#!/bin/bash

cd Discovery &&
docker-compose up -d &&
cd ..

for service in Front Comments Users Articles
do
	cd $service/src &&
	composer install &&
	cd .. &&
	docker-compose up -d &&
	cd ..
done