#!/bin/bash

for service in Front Comments Users Articles Discovery
do
	cd $service &&
	docker-compose down &&
	cd ..
done